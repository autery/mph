# Mph

## Installation

Once the tarball is extracted, change to the mph directory, then run bundler to download any missing dependent gems:

```bash
$ cd mph
$ bundle
```


## Usage

```bash
./bin/mph <file>
```

...or...

```bash
cat <file> | ./bin/mph
```

## Sample data

`spec/fixtures/sample.txt` contains the example input from [Dan's gist](https://gist.github.com/dan-manges/1e1854d0704cb9132b74). Running `./bin/mph spec/fixtures/sample.txt` should produce the expected output from the gist:

```bash
$ ./bin/mph spec/fixtures/sample.txt
Alex: 42 miles @ 34 mph
Dan: 39 miles @ 47 mph
Bob: 0 miles
```

## Development Process

I chose to implement my submission in ruby, but this was not so complicated a problem as to require rails, so I just used `bundle gem` to create the scaffolding. Despite not using rails, it still made sense to use FactoryBot to mock some sample data.

Handling speed calculations sanely was a concern. I picked `Time.parse` to turn hh:mm fields into objects I could do math on. Subtracting 2 Time objects gives you a delta in seconds, which felt better to me than worrying about rounding problems by the naive solution of just keeping the trip lengths in hours with `hh + mm/60`.

The problem required speed checks on individual trips to make sure their range is between 5 and 100 mph, and also on driver's average speed over all his/her trips. So at the risk of being "enterprisey", I implemented the simple mph calculation as a class method in a separate file.

The choice of requirements in this exercise lent itself very well to ruby. The `ARGF` class easily flips between reading files and reading STDIN, depending on what the commandline contained. The input lines being space-delimited naturally leant itself to tricks like:

```ruby
action, *fields = line.split
```

For modeling the domain of the problem, I saw it as three parts:

* A "parser" (perhaps that's the wrong word) to turn the input set into an array of drivers
* A "driver" class, each instance containing an array of trips, and reduction methods to turn the trips into the parts we care about: total time, and total mileage
* A "trip" class, which is just a property bag for fields from the trip input lines

With this model, decision-making can be managed by three rules:

* Trips are valid if they are between 5 and 100 mph. To determine this, the `trip` class has a `valid?` method.
* Trips are added to a driver's list only if they are valid, with a simple `trips.push trip if trip.valid?`.
* When casting a driver to a string, average MPH is only included if valid trips exist for them... which I'm determining by checking if the total mileage is greater than zero.

## Testing

A simple `rspec spec/` will run a handful of tests I thought would best demonstrate the "correctness" of the app.

* mph_spec checks that the gist input produces the gist output, and throws no errors.
* driver_spec checks that drivers with and without trips have the expected string conversions (either with or without an average mph), and that both too slow and too fast trips don't get included in the trips list.
* trip_spec checks that trips produce the expected time deltas. This could arguably be fleshed out a little more, and, at the risk of being presumptious, maybe my pair and I will add to that during an in-person interview later. I sort of don't like the fact that I'm only checking the `valid?` operation by inference from a check in driver_spec.
* FactoryBot mocks all the objects, with a few traits determining what to initialize the classes with.

## TODO

This is a brittle implementation. If a trip is introduced before it's driver is defined, the whole script will exit and throw a no method error. In a real project, I'd want to speak with the product owner and see how he/she would want to handle that. Maybe throwing an exception is correct, maybe we sort the input to put drivers first, or maybe we deliberately drop lines that are unprocessable at the time they were read. That decision would also inform what sort of test I would write to capture the intent.

I'm not very happy with "parser" as a name for something that is essentially a controller. I'd expect a code review or the partner I paired with to surface that as a code smell, and the two of us would find an alternative, whether it's a simple rename, or a refactor to split out functionality. It's been several years since I took code reviews as a challenge to defend my choices. Now I look forward to them to help me find different ways of doing things, which a second pair of eyes on the problem will usually do. Code reviews and pair programming are both like improv comedy - they work best if you always say "yes".
