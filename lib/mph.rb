require "mph/version"
require "mph/parser"

# Mph takes a list of lines in the format:
#   Driver Dan
#   Trip Dan 07:15 07:45 17.3
#
# and returns a list sorted in descending order of driver total distance, in the format:
#   Alex: 42 miles @ 34 mph
#   Dan: 39 miles @ 47 mph
#   Bob: 0 miles
module Mph
  def self.run lines
    parser = Parser.new
    lines.each { |line| parser.parse line }
    puts parser.drivers.sort_by { |driver| driver.total_miles }.reverse
  end
end
