require "Time"
require "mph/speed"

module Mph
  # Trip provides a method to take a start and end time in the format "hh:mm", and return
  # the number of seconds the trip took, for use in MPH calculations
  class Trip
    attr_reader :trip_start, :trip_end, :miles

    def initialize trip_start, trip_end, miles
      @trip_start = trip_start
      @trip_end = trip_end
      @miles = miles.to_f
    end

    def total_seconds
      (Time.parse trip_end) - (Time.parse trip_start)
    end

    def valid?
      Speed.mph(total_seconds, miles).between? 5, 100
    end
  end
end

