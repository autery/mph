require "mph/trip"
require "mph/driver"

module Mph
  # Parser creates an array of drivers and their associated trips
  # given an input file with lines in one of these formats:
  # Driver Dan
  # Trip Dan 07:15 07:45 17.3
  class Parser
    attr_reader :drivers
    def initialize
      @drivers = []
    end

    def parse line
      action, *fields = line.split
      if action == 'Driver'
        add_driver fields.first
      elsif line.start_with? 'Trip'
        add_trip fields
      end
    end

    def add_driver name
      driver = Driver.new name
      drivers.push driver
    end

    def add_trip fields
      name = fields.shift
      drivers.find { |driver| driver.name == name }.add_trip (Trip.new *fields)
    end
  end
end

