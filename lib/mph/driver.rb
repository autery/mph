require "Time"
require "mph/speed"

module Mph
  # Driver associates trips with a name.
  # It provides methods for calculating total distance and average speed for a driver's trips
  class Driver
    attr_reader :name, :trips

    def initialize name
      @name = name
      @trips = []
    end

    def add_trip trip
      trips.push trip if trip.valid?
    end

    def total_miles
      total = trips.map(&:miles).reduce(&:+) || 0
      total.round
    end

    def total_seconds
      trips.map(&:total_seconds).reduce(&:+)
    end

    def average_mph
      Speed.mph(total_seconds, total_miles).round
    end

    def to_s
      if total_miles > 0
        "#{name}: #{total_miles} miles @ #{average_mph} mph"
      else
        "#{name}: #{total_miles} miles"
      end
    end
  end
end

