require "Time"

module Mph
  # Given total seconds and miles traveled, returns average speed in miles per hour
  class Speed
    def self.mph seconds, miles
      hours = seconds / 3600
      miles / hours
    end
  end
end
