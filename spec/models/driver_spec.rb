require "spec_helper"

RSpec.describe Mph::Driver do
  before(:each) do
    @driver = build :driver
  end

  it "drivers without trips have no average mph" do
    expect(@driver.to_s).to eq "Bob: 0 miles"
  end

  it "drivers with trips contain average mph" do
    trip = build :trip
    @driver.add_trip trip

    expect(@driver.to_s).to eq "Bob: 60 miles @ 60 mph"
  end

  it "only adds trips averaging between 5 and 100 mph" do
    slow_trip = build :trip, :slow
    fast_trip = build :trip, :fast
    @driver.add_trip slow_trip
    @driver.add_trip fast_trip
    expect(@driver.trips.length).to eq 0
  end
end
