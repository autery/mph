require "spec_helper"

RSpec.describe Mph::Trip do
  let(:trip) { build :trip }
  let(:short_trip) { build :trip, :short }

  it "trips calculate seconds delta correctly" do
    expect(trip.total_seconds).to eq 60 * 60
  end

  it "1 minute trips take 60 seconds" do
    expect(short_trip.total_seconds).to eq 60
  end
end
