RSpec.describe Mph do
  it "has a version number" do
    expect(Mph::VERSION).not_to be nil
  end

  context "with test data from spec/fixtures" do
    let(:data) { File.readlines(File.join('spec', 'fixtures', 'sample.txt')) }

    it "produces correct output" do
      expected = <<~END
        Alex: 42 miles @ 34 mph
        Dan: 39 miles @ 47 mph
        Bob: 0 miles
      END

      expect {Mph.run data}.to output(expected).to_stdout
    end

    it "runs error-free" do
      expect {Mph.run data}.not_to output.to_stderr_from_any_process
    end
  end
end
