FactoryBot.define do
  factory :trip, :class => Mph::Trip do
    trip_start "00:00"
    trip_end   "01:00"
    miles      "60"

    trait :short do
      trip_end "00:01"
      miles "1"
    end

    trait :slow do
      miles "4"
    end

    trait :fast do
      miles "101"
    end

    initialize_with { new(trip_start, trip_end, miles) }
  end
end
