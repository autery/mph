FactoryBot.define do
  factory :driver, :class => Mph::Driver do
    name "Bob"
    initialize_with { new(name) }
  end
end
